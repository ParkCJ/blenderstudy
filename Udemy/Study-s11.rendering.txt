===========================================
1, 2. Basics of lighting in blender, rendering
===========================================
예쁘게 렌더링 하기위해, 3 point light와 hdri 이미지를 설정한다.

1. 3 point lighting 설정
1) Shading 모드 선택 > 왼쪽 상단창 3D viewport로 변경 (numkey:7 topview 설정)
2) 바닥 생성 (plane) > material 생성 (color: 밝기 0.25, roughness: 0)
3) light 배치
	key light: light 선택 > object data properties > light/sun 선택 > strength: 3 > color: 살짝 노란색
		> 바닥의 앞 오른쪽에 위치 (왼쪽 상단 3d viewport에서 확인) > rotate (캐릭터를 향하도록)

	feel light: light 선택 > object data properties > light/sun 선택 > strength: 1 > color: 살짝 빨간색
		> 바닥의 앞 왼쪽에 위치 (왼쪽 상단 3d viewport에서 확인) > rotate (캐릭터를 향하도록)

	back light: light 선택 > object data properties > light/sun 선택 > strength: 4 > color: 살짝 파란색 (key, feel light보다는 조금 진하게)
		> 바닥의 뒤 가운데에서살짝오른쪽에 위치 (왼쪽 상단 3d viewport에서 확인) > rotate (캐릭터를 향하도록)

2. HDRI 설정
HDRI 다운로드: Poly Haven 접속 (https://polyhaven.com/) > 다운로드 (1k, 2k로 충분. 영화, 필름등은 8k) > World에 적용

3. Render properties 설정
1) 엔진변경: cycle > eevee (최종 결과는 cycle로 만들더라도, 만들때는 eevee로 만든다)
2) Ambient occlusion 체크 > Distance를 크게 (0.25. 너무 올리면 이상한 그림자등이 생긴다)
3) Screen space reflection 체크 (물체가 철판등에 반사되서 보이는가 여부) > Half res trace 체크 해제
4) Film: transparent 체크 (Hdri배경이 물체에는 비치게 하나, 배경 자체는 보이지 않게 한다)

4. 바닥 크기를 작게 (orc가 걷는 정도만 잘 보이게)
5. 카메라 흔들림 주기: 발이 땅에 닿을 때 카메라가 살짝 흔들리게 하기 (양발 모두 적용)
dope sheet > 발이 땅에 닿을때: 카메라 location 저장
	> 다음 프레임: 카메라를 살짝 움직여서 저장
	> 다음 프레임: 카메라를 반대쪽으로 살짝 움직여서 저장
	> 다음 프레임: 발이 땅에 닿을때와 같이 원래 location 저장

6. 렌더링
1) End 프레임: 애니메이션에 맞게 설정
2) Frame rate: 30 (Maximo에서 가져온 애니메이션이 30fps 이므로 거기에 맞춘다)
3) Output 폴더 설정
4) 제대로 렌더링 되는지 확인: png로 한장 렌더링해서 테스트
5) ffmpeg로 변경 > 애니메이션 렌더링

7. 기타
오리지널 코드 (이전버전) 접속 방법:
https://gdev.tv/bcc-archive 접속 > confirmation page > Enrol (비밀번호: archive)

https://sketchfab.com/: 3D 모델 스토어 (웹상에서 회전시키며 볼 수 있다. 참고용)