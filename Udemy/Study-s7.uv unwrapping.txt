===========================================
1. Understanding UV Unwrapping
===========================================
* Fac (Black and white)
Black: 0%, White: 100%
ex) Roughness: Black > No roughness

1. UV Editor
1) 축: X, Y축으로 구성
2) Editor 윈도우 처럼 G,S,R 사용가능
3) UV Sync Selection (UV Editor 상단 왼쪽):
UV Editor 윈도우와 Editor 윈도우의 '점,선,면' 선택을 동기화한다.

2. UV Map 만들기
Default로 UV Map이 존재
Object의 Scale을 변경한 경우, 지우고 다시 만들어야 한다.

1) Scale을 변경한 경우
스케일 적용: Ctrl + A > Scale
> Default UV Map 삭제: Object data properties/UV Maps > Default UV Map 삭제
> Unwrap: Editor 모드 > 전체 선택 > U > Unwrap (결과: UV Editor에 모든 Face를 Full size로 겹쳐서 펼쳐놓는다)
또는 U > Smart UV Project (결과: UV Editor에 모든 Face를 적절히 분배해서 펼쳐놓는다)

===========================================
2. Marking Seams
===========================================
* Seam은 가능한 눈에 안보이는 곳으로 (Seam 부분은 텍스쳐가 만나는 부분이므로, 가능하면 눈에 보이지 않는 곳에 만든다.)
* Island 수를 최대한 적게 (Island: UV map에 존재하는 각각의 조각. UV map을 만들때는 Island 수를 최대한 적게 한다.)
* Stretching 되는 부분을 최대한 적게 (Uv 에디터 > N > View > Display > Stretching: UV Map 중에서 stretching 된 부분을 옅은 파랑색으로 보여준다.)
* Object의 UV를 하나의 이미지 (여러개의 이미지가 아닌)에 펼쳐놓는게 성능상 좋다.

* Edge 메뉴 (=Ctrl + E)

1. 창 구성하기
왼쪽 상단: Shader editor
왼쪽 하단: UV editor
오른쪽: 3D viewport

2. 벨트의 UV map 생성
벨트 뒷면 없애기 (안보이는 부분): Isolation 모드 > Edit 모드 > 벨트 뒷면 선택 > Face 삭제
Seam 만들기: Edit 모드 > 왼쪽 옆구리 부분에 Seam 생성 (Ctrl + E > Mark seam)
UV map 만들기: Edit 모드 > 전체 선택 > U > Unwrap (> 스트레칭 확인 > 문제 있으면 Seam 조정 > Unwrap. 반복)

3. 왼쪽 뿔의 UV map 생성
Seam 만들기: Edit 모드 > 뒷쪽 부분에 Seam 생성
UV map 만들기: Edit 모드 > 전체 선택 > U > Unwrap (> 스트레칭 확인 > 문제 있으면 Seam 조정 > Unwrap. 반복)

===========================================
3. Marking Seams 2
===========================================
1. 오른쪽 뿔의 UV map 생성 (실린더 모양 같으므로, 옆과 뚜껑 모양으로 seam 생성)
2. Loin cloth (앞)의 UV map 생성
3. Loin cloth (뒤)의 UV map 생성
4. 벨트의 버클 UV map 생성
5. Shackle의 UV map 생성
6. Shackle의 고리들 UV map 생성
7. 이빨 UV map 생성

===========================================
4. Unwrapping the body
===========================================
* 팔, 다리, 손가락, 발가락은 실린더로 생각
* 급경사가 있는 곳은 seam 생성
* 성능 향상 위해 Mirror 이용 (Mirror on 상태에서 UV map 생성)
UV map이 반대쪽에 그대로 적용된다 -> Paint 하면 양쪽이 같은 색이 됨
모바일 게임등에서 성능을 위해 일부러 사용
그 외 경우는 UV map 생성 후, Mirror 적용하고 다시 한번 UV map 생성 (Mirror on 상태에서 seam 생성해야 빨리 작업 가능하므로)
* UV editor 전체화면으로 보기: ctrl + space

* seam으로 나누어진 부분까지만 전체 선택: Selected linked (선택할 부분에 마우스 올려두고 L): seam을 선택 > 커서 올리고 L (다른 곳에 이동해 다시 L 눌러 동시에 여러면 선택 가능)

1. 팔, 손 UV map 생성
1) 팔 UV map 생성
팔과 몸통을 나누는 seam 생성 (S1) > 팔 안쪽으로 긴 seam 생성 > 팔과 손을 나누는 seam 생성

2) 손 UV map 생성
정권부터 첫번째 마디까지 주먹 앞 부분 동그랗게 seam 생성 > 손바닥 쪽에서 팔과의 경계까지 seam 생성

3) Selected linked > seam 선택 > 팔이나 손에 커서두고 L > 팔과 손의 UV map이 잘 만들어졌나 확인

2. 머리 UV map 생성: 총 4개의 면을 생성 (얼굴, 앞 이마, 뒷통수, 귀. ref/s7.uv map-head1.jpg 참조)
1) 머리와 목 나누기: 머리와 목을 나누는 seam (S2) 생성
2) 귀 면 만들기: 귀를 감싸는 seam (H1) 생성
3) 뒤통수 면 만들기: 어깨선을 따라 S2부터 H1까지 seam (H2) 생성 > H2를 따라 정수리를 거쳐 뒷통수를 감싸도록 seam 생성 (H3)
4) 앞 이마 면 만들기: 왼쪽 귀 윗부분에서 오른쪽 귀 윗부분까지 seam 생성 (H4) > 앞 이마 면과 얼굴 면이 나눠짐
5) 결과 확인 > 스트레칭이 심한 부분은
- seam을 추가하거나
- k (나이프툴)로 더 나눈다

3. 몸통 UV map 생성 (s7.uv map-body1.jpg 참조)
몸통 앞 면, 뒷 면 나누기: 어깨선을 따라 S1과 S2를 연결하는 seam 생성 > 옆구리를 따라 S1부터 골반까지 seam 생성 > 골반부터 Groin까지 seam 생성 (앞, 뒤)

4. 다리 UV map 생성 (s7.uv map-leg1.jpg 참조)
허벅지 안쪽을 따라 Groin 부터 발바닥까지 seam 생성 > 발바닥을 둘러싸는 seam 생성

===========================================
5. Repair stretching problems
===========================================
* Triangulate faces: Quad를 모두 Tri로 만든다. (!! 모델링이 모두 끝난다음에 해야한다. Tri로 만들면 더 이상 모델링 불가능!!)

Stretching 된 부분 (Shading anomalies)를 해결한다.
1. 전체를 Tri로 변경한다: 전체 선택 > Ctrl + F > Triangulate faces
(부분만 할 경우: Stretching 된 부분 중 Quad로 되어 있는 부분을 knife 툴을 이용해 Tri로 변경한다.)
2. 남아있는 Stretching 해결: UV map 확인 > Stretching이 남아있는 부분이 있으면 > 전체 선택 > Unwrap > 해결된 것 확인
3. 그래도 남아있는 Stretching: 아주 작은 부분이면 무시한다.

===========================================
6. Unwrapping all objects together
===========================================
* 모든 Object를 하나의 UV map에 나타나도록 한다.
1. 모든 Object를 보이게 한다.
2. 스케일: 모든 Object의 Scale을 1로 설정 (그래야 Blender에서 각 Object의 크기 차이를 제대로 인식한다.)
3. Orc에 mirror modifier 적용
4. 모든 Object 선택 (눈 빼고) > Unwrap
5. UV map에서 Island의 크기 조정 (중요한 Object는 크게, 중요하지 않은 것은 작게)
6. Island repack (상단 uv > pack islands, margin (island 사이의 간격): 0.006으로)
7. UV map에서 overlap 된 부분이 있는지 확인한다 (모델링 할 때 실수해서 Face가 두개가 되었다거나): 상단 Select > Select overlap
> Overlap이 존재하는 경우 수정 (확인 후 merge by distance 등으로. 버그로 Overlap 없는데 있다고 표시되는 경우가 있다. 그럴때는 무시)