===========================================
틀 만들기
===========================================
1. 정면에 틀 만들기: 정면에 Plane 생성 > Mirror 적용 > Subdivide > 모양 만들기
2. Exclude > 뒤쪽면 만들기
3. 윗면, 옆면, 아랫면 Face 삭제 (Only face)
4. 전체를 위쪽으로 조금 올림 (Simulation시 밑으로 떨어지기 때문에)

===========================================
Simulator 동작 > 옷 모양 만들기
===========================================
1. 몸에 Collision 설정: 몸 선택 > Physics properties > Collision 활성화
2. 옷 선택 > Physics properties > Cloth 활성화
	Preset: Denim

	Cloth
		Quality step: 12 (충돌 Simulation을 얼마나 디테일하게 하는지)
		Physical Properties
			Vertex Mass: 1 kg

	Shape
		Sewing: 체크
		Max Sewing Force: 1000 (0이면 무한정으로 끌어당김)
		Shrinking Factor: 기본값. 필요시 조정 (수축의 정도. 양수: 수축, 음수: 덜 수축. 최대: 1)

	Object Collisions (다른 Object와 충돌시에 대한 설정)
		Distance: 0.001m. 실행해보면서 조정 (몸과 옷 사이의 거리)
		몸의 Collision/Thickness outer: 0.01 (몸에서 얼만큼 떨어진곳에서 충돌이 일어나는지)

	Self Collisions: 체크. 옷감들이 서로 뚫고 나가지 않게 한다. (자신의 Collision 설정)
		Distance: 0.001m (자신끼리의 Collision 거리)

	Property Weight: Default (옷안의 특정 Object에 장력을 더 주고 싶은 등 Vetex별로 설정할때 사용)

3. 애니메이션 재생 > 잘 붙은 순간 정지
4. 미세조정 > 설정 변경 > 재실행

===========================================
마무리
===========================================
1. Shade smooth
2. Mirror 적용
3. Cloth simulator 적용
4. 부족한 Face 생성
5. 튿어진 곳 붙이기 (Merge vertex by distance)
6. 몸 안으로 들어간 곳은 밖으로 꺼냄 (Propertional editing 키고)
7. Solidify로 두께 생성