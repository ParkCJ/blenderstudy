﻿==============================
기본
==============================
오브젝트 이동 = G
> 축 방향 이동 = 휠누르고
> 축 방향 이동 = X.Y.Z중 하나
> 축 빼고 이동 = Shift + 축
> 이동 취소 = Alt + G (이동을 초기화)

오브젝트 회전 = R (로컬좌표 이용시는 R 두번) <-> Alt + R (회전을 초기화)
오브젝트 회전 후 회전값0으로 = Ctrl+A > Apply Rotation
오브젝트 스케일 = S (로컬좌표 이용시는 S 두번) <-> Alt + S (스케일링을 초기화)
오브젝트 스케일을 모두 균일하게 = Ctrl+A > Apply Scale

미세하게 조정 = Shift를 누른상태로

모드 전환 (Object, Edit 모드) = Tab
생성 (Object 등) = Shift + A

3D커서 위치 이동 = Shift + 우클릭
3D커서 메뉴 = Shift + S
3D커서 기준으로 회전하기 = 3D 커서 위치 설정 > 상단메뉴 Transform pivot point > 3D 커서 선택 > 회전

(Object 모드) Shade smooth 각도 제한 = Object Data Properties/Normals/Auto smooth 체크
(Object 모드) 두개의 Object 선택 => Ctrl + J (Join) = 하나의 Object로 됨

중심점 맞추기
> 중심점에 커서 위치 (Shift + S -> Cursor to selected)
->
> 커서 위치를 중심점으로 설정 (중심점을 바꿀 Object 선택 -> Set origin -> Origin to 3d Cursor)

Shader editor 연결 자르기 = Ctrl + 오른쪽 클릭 + 드래그

Dimention: 크기, 규모 (예: 큐브의 Dimensions.x = 큐브의 X축 길이)

퀵메뉴 추가 = 메뉴에 오른쪽 버튼 클릭 > Add to quick favorites
퀵메뉴 열기 = Q

Editor type 선택 열기 = Ctrl + Tab
Editor 확대 = Ctrl + Space
Viewport 선택 열기 = Z

숨기기 = h (다시 보이기 = Ctrl + z)

단축기 찾기 = F3

Pixel axis = 그냥 axis. 미러 Modifier 사용시 중심점으로 이용. Ctrl+A > Empty > Pixel axis

부드럽게 하기 = Shade smooth > Object data properties > Normals > Auto smooth 체크 > Normal의 각도 입력
(부드러운 가운데 일부는 Sharp 하게 하기 (Normal 각도로 맘에 들게 조정할 수 없을 때 사용) = Edit 모드 > 일부의 Edge 선택 > Mark sharp 선택)

링크 연결하기 = Ctrl + L

Outliner 창에서 선택한 Object 찾기 = Object 선택 > Outliner 창에 마우스 올려두고 Numpad의 . (period)키

Bake 할 때 GPU로 계산하기
(계산 속도가 빨라진다. 그래픽카드가 이 기능을 지원할 경우. 주의: GPU 계산하면 Glitch가 발생하는 경우가 있다. Normal, cavity map에서 원인모를 Glitch가 발생하면 CPU로 계산)
= Edit > Preferences > System > CUDA > 그래픽카드에 체크
> Render properties > Device: GPU Compute 선택
==============================
Edit 모드
==============================
* Quad (사각)을 유지해야 한다.

(Edit 모드) 전,선,면 선택 = 1,2,3
(Edit 모드) Inset Face = i
(Edit 모드) Extrude = e
(Edit 모드) Extrude (individual faces) = alt + e
(Edit 모드) Extrude -> alt + s (Normal 방향으로 Scale)
(Edit 모드) Face 선택 -> alt + s (Normal 방향으로 Scale)
(Edit 모드) Edge 선택 -> Ctrl + R = 루프컷
(Edit 모드) 루프컷 옵션 (Follow, Even) = Ctrl + R -> F or E
(Edit 모드) 루프컷의 Edge 더블클릭 = 루프컷 전체 선택
(Edit 모드) 루프컷 선택 중 Shift + 다른 루프컷 더블클릭 = 여러개의 루프컷 선택
(Edit 모드) 루프컷 이동시 G를 두번 누르고 이동 = 루프컷의 크기 유지하지 않으며 이동
(Edit 모드) 전체 선택 = A
(Edit 모드) 솔로 선택 = /
(Edit 모드) X-Ray 켜기 (오브젝트+와이어프레임 같이 표시) = Alt + Z
(Edit 모드) Edge 로 둘레 선택 -> Edge split = Edge를 기준으로 두개로 분리
(Edit 모드) Edge 선택 -> L = 자신과 연결된 모든 Edge 선택
(Edit 모드) 둘레에 면을 만든다 = Edge 로 둘레 선택 -> F (New face from edge)
(Edit 모드) 두 Vertex 사이에 Edge를 만든다 = Vertex 두개 선택 -> J (Connect vertex path)
(Edit 모드) K (Knife 모드) = 엣지를 자른다. ex) 5각형일때 잘라서 4각형 2개로 만들 때
(Edit 모드) K (Knife 모드) > 두 Edge 선택 = 두 Edge 사이에 Edge를 만든다.
(Edit 모드) Edge 두개 선택 > Bridge edge loop = 두 Edge 사이에 Face (면)을 만든다.
(Edit 모드) Dissolve edge (Face는 그대로 두고 그 위에 있는 Edge만 없앤다) = Edge 선택 > Ctrl + x (오른쪽 메뉴 > Dissolve edge)
(Edit 모드) 일부 선택 -> P (Seperate) = Object를 나눈다.
(Edit 모드) Merge (합쳐주기) -> M
(Edit 모드) 두개의 Edge loop 사이에 면 만들기 = Ctrl + E (Bridge edge loops)
(Edit 모드) Shift + D = 복사 (같은 Object로 유지된다. 서로 다른 Object로 하려면, Object 모드에서 할 것) > Z = Z축으로 복사한 걸 이동
(Edit 모드) Edge 루프 선택 > S > 방향 > 숫자 0 = 반듯하게 된다.
(Edit 모드) 조인트 모두 선택 = Ctrl + +
(Edit 모드) 조인트 모두 선택 취소 = Ctrl + -
(Edit 모드) 기울이기 (Shear) = Ctrl + Alt + Shift + S
(Edit 모드) 베블 = Ctrl + B
(Edit 모드) Merge vertex = m (가까운 거리에 있는 vertex를 모두 merge = 모든 vertex 선택 > m > by distance > distance 조정)
(Edit 모드) 복제 (Object: 서로 관계없는 Object가 된다) = Shift + D
(Edit 모드) 복제 (Link: Object에다가 Link까지 복제한다. 하나의 Object가 변경되면, 모든 Object에 반영된다) = Alt + D
		<-> 복제 (Link) 해제하기: Object > Relations > Make single user > Object & Data 선택
(Edit 모드) 같은 위치로 이동한 vertices를 모두 자동으로 머지 = Auto merge vertices (신 우측 상단 메뉴의 options 왼쪽)

반전 선택 (현재 선택한 것을 제외한 나머지를 모두 선택): 면 선택 > Ctrl + I
숨기기: 면 선택 > H
숨긴것 보이게 하기: Alt + H

==============================
머리카락
==============================
머리카락 꼬기: Pivot 포인트 (각각) > Path의 정점선택 > Ctrl + T > 마우스 드래그
Grid Fill: Edge 선택 > Ctrl + F > Grid Fill

==============================
Shading
==============================
(Viewport shading) 빛 사용하기 = 오른쪽 삼각마크 > Lighting > Scene Lights에 체크
(Viewport shading) Eevee 그림자 표현 더 확실하게 = Render properties > Ambient occlusion 체크 > Distance를 크게 (3정도. 너무 올리면 이상한 그림자등이 생긴다)
(Viewport shading) Eevee 에서 Emission 표현하기 = Render properties > Bloom 체크 > Intensity를 크게 (0.075정도)
(Viewport shading) 물체가 철판등에 반사되서 보이게하기 = Screen space reflection 체크 > Half res trace 체크 해제
(Viewport shading) Hdri배경이 물체에는 비치게 하나, 배경 자체는 보이지 않게 하기 = Film > transparent 체크
(Viewport shading) 배경 이미지 만들기 = Shader editor > Shader type을 World로 > Shift + A > Environment texture > 이미지 선택

Emission material 만들기 = 기본 BSDF 제거 > Shift + A > Shader > Emission 선택 > Stre를 10으로
Color grid 사용하기 = 아래 오른쪽 창: Material을 적용할 Object 선택 > Material 생성 > 이름을 color grid material로 > Image texture 생성 (color grid 선택) > BSDF에 연결
		      -> 아래 왼쪽에 UV Editor 창 생성 > 메모리에서 color grid 선택
		      -> 위 창: Material을 적용할 Object 선택 > Edit 모드 > 전체 선택 (왼쪽 UV Editor에 표시됨) > 왼쪽 UV Editor에서 전체 선택 > 작게 만들어서 하나의 색 Grid에 넣음
		      =>
		      -> 전체중 일부의 색 변경: Edit 모드 > Face 선택 > UV Editor에 선택된 Face만 표시됨 > UV Editor에서 선택된 Face만 다른 색으로 옮김
		      -> 전체중 일부 Emission: Slot 추가 > Image texture (Color grid)-Emission-Material out으로 연결 > Edit 모드 > Face 선택 > UV Editor에서 색 변경

==============================
카메라
==============================
카메라 위치 조정 = N > View > View lock > Lock camera to view

==============================
동영상
==============================
회전 동영상 촬영
1. Circle 만들기 = Shift + A > Curve > Circle (pivot을 median point로)
2. 카메라를 Circle에 올리기 = Circle과 카메라 선택 > P > Follow path
3. Circle의 Length (한바퀴 도는데 걸리는 Frame) 설정 = Circle 선택 > Object data property > Path animation 체크 > Frames를 250으로
4. 카메라의 타겟 설정하기 = Shift + A > Empty > Arrows 생성
				=> 카메라 선택 > Constraint property > Add constraint > Track to 선택 > Arrows 선택 > To: -Z, Up: Y로 설정
				=> 대상이 잘 보이게 Arrows의 위치 조정
5. 퀄리티 올리기 = Render properties > Screen space reflections 체크 > Half res trace 체크 해제
6. 배경을 투명하게 하고 싶으면 = Render properties > Film > Transparent 체크
7. 저장하기 = Output properties > Dimensions 설정 (해상도, Frame rate 등) > Frame rate를 25로
				=> Output 설정 > 폴더 선택 > File format을 ffmpeg video로 설정
				=> 상단탭 > Render > Render animation

==============================
리깅
==============================
FK = Parent 애니메이션
IK = 반대

자식 부모 관계 설정
> 자식 선택 -> 부모 선택 -> Ctrl + P (Set Parent) -> Object 선택

Transform 선택 -> I = Key frame 추가

리깅 = 캐릭터에 뼈대를 심는 과정
웨이트 = 버텍스마다 어떤 뼈에 얼만큼 영향을 받아야 하는지 결정하는 것
뼈대가 항상 위에 보이게 하기 = Object properties > Viewport display > In front 체크
연결된 새로운 뼈 만들기 = Edit mode > Exclude

Rigging 하기
1. 뼈 만들기 (Shift + A > Armature)
   - 뼈 항상 위에 보이기 (Object Properties > Viewport Display > In Front 체크)
2. 오브젝트를 뼈 (Armature)의 자식으로 설정 (자식부터 선택 후 Ctrl + P)

Weight paint 모드 (Object와 뼈대 모두 선택 후) = 웨이트 확인
   뼈 선택하기 = Ctrl + 뼈 선택
   Zero weight 검게 표시하기 = Overlays > Weigt points > Zero weights > Active
   뒤의 안보이는 부분까지 색칠 = Brush settings > Advanced > Front faces only 체크해제, 그리고
					   > Falloff > Falloff shape > Project 선택
   Mirror 부분까지 색칠 = Brush settings > Symmetry > X, Y, Z 선택
   Weight 리셋 = 상단 weights > Assign automatic from bones 선택
   Weight 부드럽게 = 상단 weights > smooth 선택

IK_CTRL 뼈 만들기
1. 뼈를 생성 후, 연결된 뼈가 아닌, 가장 중심이 되는 뼈를 부모로 한다.
2. Bone properties > Deform을 체크해제한다. (Mesh를 움직이는 뼈가 아니므로)
3. 나에게 연결된 뼈를 선택한 후, Bone constraint (Inverse kinematics) 를 추가
4. Chain length 설정 및 필요시 Rotation 체크 (IK의 Rotation도 영향을 주게 한다.)
5. Pole 뼈를 생성 후, 연결된 뼈가 아닌, IK_CTRL 뼈를 부모로 한다.
6. Inverse kinematics에서 Pole 뼈 설정
7. Inverse kinematics에서 Pole angle 설정

IK를 사용해서 애니메이션을 만들때 = IK 뼈만 움직인다. 다른 뼈는 건들지 않음
Pole 관절 부분을 회전시킬 때 = Pole 을 회전시키면 따라 회전한다
부모 자식 관계는 유지하면서, offset 벌리기 = Alt + P > Disconnect bone

팔, 다리 만들기 (속성/X-Axis mirror을 이용해서 한쪽만 만들면 다른쪽이 같이 만들어지게함)
	      > 머리뼈에서 > 속성/X-Axis mirror 체크 > Shift + E 로 Extrude (양쪽으로 뼈가 튀어나옴)
	      > 하나만 선택한 상태로 Alt + P > Clear parent 로 부모해제
	      > 하나만 선택한 상태로 위치 이동 (끝이 팔의 시작점에 위치하도록) > Parent 연결
	      > 여기까지하면 쇄골뼈 완성 > 쇄골에서 팔 뼈를 만들어냄

==============================
애니메이션: 큰 동작부터 세부적인 동작으로 좁혀나가며 만든다.
==============================
키프레임 애니메이션 보간방식
	등속운동 (리니어): 속도가 일정. 예) <1> - 2 - 3 - 4 - <5>
	가속, 감속운동 (베지어): 속도가 변함. 수직 90도에 가까울수록 빠르게 움직임. 가로 90도에 가까울수록 느리게 움직임. 예) <1> - 2,3 - 4 - <5>
	정지 (콘스탄트): 갑자기 바뀜. 예) <1> - 1 - 1 - 1 - <5>

키프레임 추가하는 방식
	1) I를 눌러서 그때그때 추가하기
	2) Auto keyframe을 활성화 (녹화)하기: 자동이지만, 종종 불편한 일도 발생함

키프레임 삭제하기 = 속성탭에 오른쪽 마우스버튼 누르고 Clear keyframes

타임라인
그래프에디터: 안볼거의 눈을 다 꺼서, 조정할거 딱 한개만 켜두고 작업하기 (여러개 한번에 끌 때는 눈 하나 클릭해서 끈상태로 드래그)
	보간방식 선택 = 드래그해서 키 프레임 선택 > T
돕 시트

Disney animation rules (디즈니 애니메이션의 12가지 법칙): https://www.youtube.com/watch?v=uDqjIdI4bF4

1) 움직일 때 뒤로 젖혀지는 동작 만들기
Cube 만들고
-> 구부러질수있게 만듬 = Edit mode > Subdivide
-> 중심점을 바닥으로 변경 = Object mode > 3D 커서를 바닥으로 > 중심점을 3D 커서위치로 변경
-> 구부러지는 Modifier 적용 = Object mode > Modifier > Simple deform (축과 각도를 기준으로 변형시킨다) > Bend
==============================
UV 매핑 = 물건을 종이로 바르는 것
==============================
1) 물체를 평면으로 펼친다. (잘라야 할 부분 표시)
	- Seem 생성 (Edge 선택 -> Mark seem)
	-> Unwrap (전체선택 -> U -> Unwrap)
	-> UV의 크기를 균등하게 (UV Editor에서 A -> UV -> Average islands scale)
	-> 재배치 (UV -> Pack islands)

2) 펼친 이미지를 저장하고 그림을 그려준다.
	- 블렌더 내에 Image 생성 (UV Editor 상단 -> NEW -> 이름넣고 저장). 주의) 실제 파일로 저장되지 않음
	-> 실제 파일로 저장 (UV Editor 상단 -> 이미지 -> Save As -> png파일)

3) 펼친 이미지를 물체에 입혀준다.
	- Shading 으로 워크스페이스로 변경
	-> 머티리얼 선택
	-> Image texture (펼친 이미지로) 를 연결

4) 그림을 그린다. 또는 그림이 그려진 이미지를 적용한다.
	그림을 그려보자.
	-> Texture Paint 워크스페이스로 변경
	-> 그림을 그리기 (브러쉬 크기 조절은 [, ] 버튼, 속성/Symmetry = 한쪽만 그려도 좌우 미러처럼 그려지는 것)
	
	포토샵으로 그림을 그려보자
	-> UV Editing 워크스페이스로 변경
	-> 오른쪽에서 전체 선택 후, 왼쪽에서 UV/Export UV Layout 선택 -> 도면 png 생성
	-> 포토샵에서 '펼친 이미지'에 '도면 이미지'를 위치시켜 어디에 그림을 그릴지 확인하고, 그림을 편집
	-> '펼친 이미지'를 저장
	-> Shading에서 Image texture 갱신

==============================
Modifier
==============================
Array = 복사를 만들어낸다. (갯수 또는 길이 또는 커브 로 설정 가능. 복사본 사이의 offset 설정 가능) (트랙)
Bool = Object에서 Object 빼기 (더하기)
Curve = Curve object를 사용해서 메쉬를 구부린다.
	Curve위에 Mesh를 올릴때도 사용. Curve와 Mesh의 방향이 일치해야 한다.
	Deform axis는 X 로 둘 것 (트랙)
Subdivision Surface = 실제로 메쉬를 나누지 않고 올렸다가 줄였다하며 모양 확인이 가능. 완성되면 Apply.
	Edge 둘레 선택 -> N -> transform/Mean crease: Subdivision의 영향을 얼마나 받는지 설정 (1이면 영향 X)
Solidify = 두께를 만들어줌 (Thick으로 두께 조정) (트랙)
ShrinkWrap = 어딘가에 딱 붙힘 (눈썹 등 만들때)
Simple deform = 중심점과 특정부분과 축을 기준으로, 각도만큼 변형시킨다
	Twist = 꽈배기처럼 꼰다
	Bend = 구부리기
	Taper = 한쪽을 점점 가늘게 표현
	Stretch = 늘리기
	
	Limits = 특정부분 설정. 기본 0~1. 예) 0.5 ~ 1 = 중간부터 꼭대기까지 부분만 변형시킴

==============================
Texture
==============================
Bump map = Mesh 변경 X. 그림자를 어떻게 표현해줄것인가를 결정. 흑백으로 표현. 단순히 높낮이만을 표현. 가장 오래된 방식.
Normal map = Mesh 변경 X. 그림자를 어떻게 표현해줄것인가를 결정. RGB로 표현. 높낮이뿐만이 아니라, X, Y 좌표까지 표현 가능
Displacement map = Mesh 변경 O (유니티의 Height map에 대응되는 듯)

Roughness
0 (흑백) = 반사율 100
100 (흰색) = 반사율 0

무료 material 다운로드 = https://polyhaven.com/textures
1) 설명
col (diffuse): 컬러
rough: 반사
normal: 그림자 방향
displacement (height): 실제 mesh의 높낮이

2) 적용순서 (무늬 같은걸 표현할 경우는, displacement까지 사용할 필요 없다.)
Material 만들기 > Shading editor 열고 >
col 적용 = Image texture - Bsdf의 Base color
rough 적용 = Image texture - Bsdf의 Roughness
normal 적용 = Image texture - Normal map - Bsdf의 Normal
	> Normal map의 strength로 조절 (그림에 그림자가 표시되는 걸 확인할 수 있다.)
displacement 적용 = Image texture - Displacement의 height - Material output의 displacement
	> Render engine를 cycles로 변경
	> Meterial properties / Settings / Displacement = Displacement only 또는 Displacement and Bump 로 변경
	> Subdivide = Mesh가 휘어질수 있게 충분히 나눈다.

==============================
툰 쉐이딩
==============================
특징:
- 빛과 그림자의 경계면이 뚜렷하다. 중간값이 없다.

BSDF (...Function): 물체와 빛의 상호작용 (밝은 부분, 어두운 부분)을 계산하는 함수
예) Image texture - BSDF - Material = Image texture가, BSDF 연산을 거쳐 pixel별로 색이 변경되어, Material에 적용된다.

Shader to rgb: BSDF의 계산 결과를 RGB로 변경 (Render engine: eve 모드에서만 작동)
Color ramp: RGB 결과를 토대로 색을 입혀줌 (Constant로 딱딱 끊어서)

1) 적용순서 (color map을 사용하지 않는 경우)
BSDF - Shader to rgb - ColorRamp (옵션을 constant로 변경. 색 단계를 나눔) - Material

2) 적용순서 (color map을 사용하는 경우: Color맵과 빛계산을 따로 만들어 합성해준다)
(color 맵) Image texture
						↘
							Mix RGB (Blending mode = Multiply, Fac 조정) - Material
						↗
(빛 계산) Diffuse BSDF - Shader to rgb - ColorRamp

3) Outline 만들기 (같은 Object를 하나 만들어 Normal을 뒤집는다)
복제하기 = Object 선택 > Shift + D > 원래자리에 놓고 > Scale로 조금 크게 한다. (이 크기에 따라 윤곽선의 두께가 결정됨)
->
Normal 뒤집기 = 복제한 Object 선택 > 기존 Material 삭제 > 새 Material 생성 > Edit 모드 > (중요!!!)전체선택 > Ctrl + Shift + N (Recalculate normal) > Inside 체크
->
색 변경 = Object 모드 > Base color = 검은색 > Material properties / Backface culling 체크
->
Material의 빛계산 삭제 = Shading editor > Bsdf 삭제

4) Color map + Normal + Displacement + 툰쉐이딩 하기 + 툰쉐이딩 아웃라인
(color 맵) Image texture
								↘
									Mix RGB (Blending mode = Multiply, Fac 조정) - Material
								↗
(normal 맵) Image texture - Diffuse BSDF - Shader to rgb - ColorRamp

Displacement map = 속성/Texture > New > Texture 이름짓기 > Open > Displacement map 선택
		   > Modifier > Displace > Dropdown 박스에서 방금만든 Texture 선택 > Texture coordinate = UV > Strength로 얼마나 튀어나올지 조절 가능
		   > Subdivide = Mesh가 휘어질수 있게 충분히 나눈다.

아웃라인 = 아웃라인 만들기 똑같이 하고 > Displace modifier > Direction = Z > Midlevel 조정으로 Outline이 잘 보이게 한다.

==============================
캐릭터 모델링
==============================
토플로지 = Edge의 흐름
	Hightlight 에 영향이 크다
	수정을 쉽게 한다. (루프컷이나, Subdivide를 통해서)

Face의 종류 = Tri (세개 엣지), Quad (네개), N-Gon (여러개)

토플로지를 잘 구성하기 위한 모델링 원칙
1. Low poly로 시작 (Edge의 흐름을 통제하기 쉽기 때문) -> 세분화해 나간다.
2. Quad로 모델링 (Edge의 방향이 명확해서, 루프컷, Subdivide를 원할하게 할 수 있다.)
	큐브로 시작

사람의 얼굴을 만들때 토플로지
1. 근육의 흐름을 잘 생각한다. (Edge들이 정확한 방향으로 수축, 늘어나야한다.)
	1) 눈썹부터 콧등까지 안경모양으로 한 덩어리
		눈 깜빡깜빡 위해, 눈 주변의 토플로지
	2) 코 옆에서 턱까지 내려오는 한 덩어리 (팔자주름 근육 포함)
		코는 많이 안움직인다.
		입은 다양하게 움직인다. 입 움직임의 토플로지
	3) 이마, 볼은 일반적인 토플로지

얼굴 만들기 순서 (외우는게 아니다. '토플로지를 생각하며' 응용해서 만든다. 아기도 만들어보고 다양한걸 만들어본다.)
1. 얼굴 형태 만들기
2. 눈 영역 만들기
	눈썹, 콧등의 흐름 만들기
	미간, 코 부분 영역 잡기
	눈 주위 흐름 만들기
	눈 다듬고 (눈의 세로 중앙이 Z=0에 오게 만든다)
	옆에서 코 눌러주고
3. 코, 입 영역 만들기
	코, 입까지의 흐름
	입 주위 흐름 만들기

4. 코 만들고 정리
	코 만들기
	입 구멍 만들기
	너무 큰 Face를 가진 애들은 Loop cut으로 나눠주기
	Smooth 브러쉬로 부드럽게

5. 세분화
	입술 주변 세분화
	눈알 만들기
	Elastic deform 으로 수정
	귀 만들기
		위아래 뾰족 만들기
		귀 뒷부분 튀어나온 거 만들기
		귀 앞부분 움푹하게 만들기
		귀 움푹한 부분 튀어나온 곳 2곳 만들기
	입 안쪽 공간 만들기
	눈썸 만들기
	머리카락 만들기

6. Material 적용
	살색 적용할때는 Base color에 Subsurface 사용

7. 기타
	틈틈히 Vertex의 위치를 잡아준다.
	흐름을 분리할때는 Loop cut으로 나눠준다.

상체 만들기 순서
1. 목 만들기
	머리 중, 목 만들 부분의 Face 지우기
	목 만들기
	목 원형으로 만들기 (Loop tool (Add on) 사용)

2. 어깨라인 만들기 (E > 스케일)
3. 몸통 만들기
	몸통 만들기
	가슴 라인 만들기
4. 어깨 만들기
5. 팔 만들기
	윗팔 뽑기
	팔 원형으로 만들기
	어깨, 윗팔 둥그스럼하게 만들기 (Edit 모드 > X-Ray 모드 > Lasso 툴로 어깨, 윗팔 선택 > 상단 Vertex / Smooth vertices)

6. 몸통 정리하기 (Scrulpt 모드 > Smooth)
7. 쇄골, 목, 승모근 흐름 만들기
	쇄골 부분 눌러주기
	목 양편 길쭉 근육 만들기
	승모근 만들기
8. 등에 척추뼈 따라서 들어간 것 만들기
9. 등의 어깨뼈 라인 만들기
10. 가슴 근육 만들기
11. 배 근육 라인 만들기
12. 전체 Subdivide > Smooth


<단축키>
정면뷰 = 1 (키패드)
측면뷰 = 3 (키패드)
뷰 반대편 보기 = Ctrl + 뷰번호 (키패드)
X-Ray모드 = Alt + Z
Propotional모드 = O (알파벳)
Edge따라 Vertex 움직이기 = GG
두개의 Face 사이의 모든 Face 선택 = Face 선택 > Ctrl + 두번째 Face 선택
Inset face = I
스냅 = Shift + Tab
머리카락 크기조절 = Alt + S (완전히 닫을 경우는 Alt + S + 0)
머리카락 방향 회전 = Ctrl + T
머리카락 닫기 = 속성 > Bevel > Fill Caps 체크